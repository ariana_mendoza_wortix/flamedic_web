import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {LoggedInCallback, UserLoginService} from "../../service/cognito.service";

declare var jQuery:any;
declare var $:any;

@Component({
    selector: 'awscognito-angular2-app',
    styleUrls: ['./secureHome.css'],
    templateUrl: './secureHome.html'
    
})
export class SecureHomeComponent implements OnInit, LoggedInCallback {

    constructor(public router: Router, public userService: UserLoginService) {
        this.userService.isAuthenticated(this);
        console.log("SecureHomeComponent: constructor");
    }

    ngOnInit() {
       
    }
    isLoggedIn(message: string, isLoggedIn: boolean) {
        if (!isLoggedIn) {
            this.router.navigate(['/home/login']);
        }
    }
}

