import {Component, OnInit} from "@angular/core";
import {LoggedInCallback, UserLoginService, CognitoUtil, Callback} from "../../service/cognito.service";
import {Router} from "@angular/router";

declare var jQuery:any;
declare var $:any;
declare var intelInput:string;

export class Stuff {
    public accessToken: string;
    public idToken: string;
}

@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './jwt.html'
})
export class JwtComponent implements OnInit, LoggedInCallback {

    public stuff: Stuff = new Stuff();

    constructor(public router: Router, public userService: UserLoginService, public cognitoUtil: CognitoUtil) {
        this.userService.isAuthenticated(this);
        console.log("in JwtComponent");

    }
     ngOnInit() {
        this.loadIntlTelInput();
        this.loadDatepicker();
    }

    isLoggedIn(message: string, isLoggedIn: boolean) {
        if (!isLoggedIn) {
            this.router.navigate(['/home/login']);
        } else {
            this.cognitoUtil.getAccessToken(new AccessTokenCallback(this));
            this.cognitoUtil.getIdToken(new IdTokenCallback(this));
        }
    }
    loadIntlTelInput(){
         $(document).ready(function () {
               $("#phone").intlTelInput();
            });
        
    }
    
    
    
    loadDatepicker(){
        $(document).ready(function () {
                
                $('#birthdate').datepicker({
                    format: "dd/mm/yyyy"
                }); 
                });
    }
}

export class AccessTokenCallback implements Callback {
    constructor(public jwt: JwtComponent) {

    }

    callback() {

    }

    callbackWithParam(result) {
        this.jwt.stuff.accessToken = result;
    }
}

export class IdTokenCallback implements Callback {
    constructor(public jwt: JwtComponent) {

    }

    callback() {

    }

    callbackWithParam(result) {
        this.jwt.stuff.idToken = result;
    }
}
