import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {UserRegistrationService, CognitoCallback} from "../../../service/cognito.service";
declare var jQuery:any;
declare var $:any;
declare var intelInput:string;

export class RegistrationUser {
    name: string;
    email: string;
    password: string;
    surnames: string;
    identity: string;   
    birthdate: Date;
    civil: string;
    job: string;
    gender: string;
    insurance: string;
    skin: string;
    weight:number;
    height:number;
    blood: string; 
    phone:number;
    company: string;
    customType:string;
    
    
}
/**
 * This component is responsible for displaying and controlling
 * the registration of the user.
 */
@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './registration.html'
})
export class RegisterComponent implements CognitoCallback {
    registrationUser: RegistrationUser;
    router: Router;
    errorMessage: string;
    intelInput: string;
    

    constructor(public userRegistration: UserRegistrationService, router: Router) {
        this.router = router;
        this.onInit();
        
    }

    onInit() {
        this.registrationUser = new RegistrationUser();
        this.errorMessage = null;
        this.loadIntlTelInput();
        this.loadDatepicker();
        this.loadScrooll();
        this.loadCountry();
        
    }
    
    onRegister() {
        this.errorMessage = null;
         this.intelInput= $("#phone").intlTelInput("getNumber");
        
        console.log(this.intelInput);
        this.userRegistration.register(this.registrationUser,this.intelInput, this);
    }

    cognitoCallback(message: string, result: any) {
        if (message != null) { //error
            this.errorMessage = message;
            console.log("result: " + this.errorMessage);
        } else { //success
            //move to the next step
            console.log("redirecting");
            this.router.navigate(['/home/confirmRegistration', result.user.username]);
        }
    }
    
    loadIntlTelInput(){
         $(document).ready(function () {
               $("#phone").intlTelInput();
            });
        
    }
    
    
    
    loadDatepicker(){
        $(document).ready(function () {
                
                $('#birthdate').datepicker({
                    format: "dd/mm/yyyy"
                }); 
                });
    }
    
    loadScrooll(){
        $(document).ready(function(){
    
    
       $(".estilo1 .caja").mCustomScrollbar({
            theme:"rounded-dark"
         });
    
    

        $("#form-registrer-Company").mCustomScrollbar({
            theme:"3d-dark"
       });
    
    
    
       $(".estilo3 .caja").mCustomScrollbar({
            theme:"light-thin"
        });
    
       $("#accordion .panel-body").mCustomScrollbar({
                    setHeight:300,
                    theme:"dark-3"
                });
    
    
    
    
     });
  }
   loadCountry(){
        $(document).ready(function(){
       
         $("#country").countrySelect();
       });
   }
    

    
}